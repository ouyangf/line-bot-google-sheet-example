# Import Google Sheet Library
from oauth2client.service_account import ServiceAccountCredentials
# Import Django or DRF
from rest_framework.decorators import api_view
from django.http.response import JsonResponse

# Import the Python Library of LINE official bot
# 載入Line官方機器人的Python套件
from linebot import (LineBotApi, WebhookHandler)
from linebot.exceptions import (InvalidSignatureError)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, TemplateSendMessage, ButtonsTemplate,
    CarouselColumn, CarouselTemplate, AltUri, ImageCarouselColumn,
    MessageAction, URIAction, PostbackAction)

import gspread
# Environment
# 取得環境變數套件
import environ
env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)

environ.Env.read_env()


# # Reads the configuration from .env file
# Gets LINE access keys
# 取得 LINE 存取的金鑰
CHANNEL_ACCESS_TOKEN = env('LINE_CHANNEL_ACCESS_TOKEN')
CHANNEL_SECRET = env('LINE_CHANNEL_SECRET')
CHANNEL_USER_ID = env('LINE_CHANNEL_USER_ID')

line_bot_api = LineBotApi(CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(CHANNEL_SECRET)

# Gets Google Sheet variables
# 取得Google Sheet變數
auth_json_path = 'google_sheet_key.json'
gss_scopes = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name(
    auth_json_path, gss_scopes)
gss_client = gspread.authorize(credentials)
sheet_key = env('GOOGLE_SHEET_ID')
worksheet = gss_client.open_by_key(sheet_key).sheet1


@api_view(['GET'])
def drf_gsheet_get_cell_value(request):
    # Get Google Sheet Id
    # 取得 Google Sheet 的編號
    sheet_key = request.query_params.get('sheet_key', None)
    if sheet_key == None:
        return JsonResponse({
            'result': 'fail',
            'msg': 'Invalid Google sheet key.'
        })

    # Get one Google Sheet
    # 取得一個Google試算表
    worksheet = gss_client.open_by_key(sheet_key).sheet1

    # Get the row and column indexes about Google sheet.
    # 取得Google Sheet的列跟欄的索引號碼(起始值為1)
    row_index = request.query_params.get('row', None)
    col_index = request.query_params.get('col', None)
    if row_index is None or col_index is None:
        return JsonResponse({
            'result': 'fail',
            'msg': 'Invalid row or column index.'
        })

    try:
        val = worksheet.cell(int(row_index), int(col_index)).value
    except:
        return JsonResponse({
            'result': 'fail',
            'msg': 'Unknown Error'
        })

    return JsonResponse({'msg': 'success', 'data': {"cell_value": val}})


@api_view(['GET'])
def drf_gsheet_tags(request):
    # Get one Google Sheet
    # 取得一個Google試算表
    data = gsheet_list_tags()
    print(linebot_all_tags())
    return JsonResponse({'msg': 'success', 'data': data})


@api_view(['GET'])
def drf_gsheet_qas_by_tag(request):
    # Get one Google Sheet
    # 取得一個Google試算表
    tag = request.query_params.get('tag', None)
    data = gsheet_query_qa_by_tag(tag)
    # print(linebot_qa_msg(tag))
    return JsonResponse({'msg': 'success', 'data': data})


def gsheet_list_tags():
    tag_values_list = worksheet.col_values(2)[1:]
    tags_list = []
    for tags_str in tag_values_list:
        tags = tags_str.split(',')
        for tag in tags:
            if tag not in tags_list:
                tags_list.append(tag)
    return tags_list


def gsheet_query_qa_by_tag(tag: str):
    qas = []
    if tag is None:
        return qas
    # List all values for the tag column.
    # 列出標籤欄位(Column Index: 2)的所有值
    question_list = worksheet.col_values(1)
    tag_list = worksheet.col_values(2)
    answer_list = worksheet.col_values(3)
    related_url_list = worksheet.col_values(4)
    gmap_url_list = worksheet.col_values(5)
    landing_img_list = worksheet.col_values(6)

    for index in range(len(question_list)):
        question_tag = tag_list[index]
        if tag == question_tag:
            qa = dict(
                tag=tag,
                question=question_list[index],
                answer=answer_list[index],
                related_url=related_url_list[index],
                gmap_url=gmap_url_list[index],
                landing_img=landing_img_list[index],
            )
            qas.append(qa)
    return qas


def linebot_qa_msg(tag: str):
    questions = gsheet_query_qa_by_tag(tag)

    if not questions:
        return TextSendMessage(text='沒有相關的常見問題')

    columns = []
    for qa in questions:
        actions = []
        if qa['related_url'] not in [None, '-']:
            actions.append(
                URIAction(
                    label='【推薦連結】',
                    uri=qa['related_url']
                )
            )
        if qa['gmap_url'] not in [None, '-']:
            actions.append(
                URIAction(
                    label='【Google地圖連結】',
                    uri=qa['gmap_url']
                )
            )
        actions.append(
            MessageAction(
                label='⏎ 返回關鍵字列表',
                text='~'
            ),
        )
        columns.append(
            CarouselColumn(
                title=qa['question'],
                text=qa['answer'],
                thumbnail_image_url=qa['landing_img'],
                actions=actions
            )
        )
    carousel_template_message = TemplateSendMessage(
        alt_text='Questions and Answers',
        template=CarouselTemplate(
            columns=columns
        )
    )
    return carousel_template_message


def linebot_all_tags():
    tags =gsheet_list_tags()
    line_tag_list = []
    for tag in tags:
        line_tag_list.append(
            MessageAction(
                label=tag,
                text='!' + tag
            )
        )

    columns = []
    group_count = int(len(line_tag_list)/2)
    existed_single_tag = len(line_tag_list) % 2 > 0
    title = '旅客常見問題？'
    text = '關鍵字：餐廳,景點'
    url = 'https://cdn.pixabay.com/photo/2017/05/30/04/02/support-2355701_1280.jpg'
    carouseColumnList = []
    
    for index in range(group_count):
        columns.append(
            CarouselColumn(
                        thumbnail_image_url=url,
                        title=title,
                        text=text,
                        actions=line_tag_list[index*2:2*(index+1)]
            )
        )
    if existed_single_tag:
        columns.append(
            CarouselColumn(
                        thumbnail_image_url=url,
                        title=title,
                        text=text,
                        actions=[line_tag_list[2*group_count], MessageAction(label=' ', text=' ')]
            )
        )

    carousel_template_message = TemplateSendMessage(
            alt_text='Carousel template',
            template=CarouselTemplate(
                columns=columns
            )
        )

    #line_bot_api.push_message(CHANNEL_USER_ID, TextSendMessage(text=str("Willis Test")))
    #line_bot_api.push_message(CHANNEL_USER_ID, carousel_template_message)

    return carousel_template_message


@api_view(['POST'])
def drf_linebot_webhook_qa(request):
    if request.method == 'POST':

        signature = request.headers["X-Line-Signature"]
        body_unicode = request.body.decode('utf-8')

        try:
            handler.handle(body_unicode, signature)
            line_bot_api.push_message(
                CHANNEL_USER_ID, TextSendMessage(text=body))
        except InvalidSignatureError:
            messages = (
                "Invalid signature. Please check your channel access token/channel secret."
            )
            return JsonResponse({'msg': messages})
        return JsonResponse({'msg': 'success'})


@handler.add(event=MessageEvent, message=TextMessage)
def handl_message(event):
    # Receive event and then reply message to the specific token instantly.
    # 即時取得訊息事件並且回覆訊息到指定的TOKEN

    # Parse the event
    # 解析事件內容
    msg = event.message.text
    msg = msg.encode('utf-8').decode('utf-8')
    line_msg = qa_sheet_machine(sheet_key, msg)

    # Reply messages
    # 回覆訊息
    line_bot_api.reply_message(
        reply_token=event.reply_token,
        # messages=TextSendMessage(text=str(qa)),
        messages=line_msg,
    )


@api_view(['POST'])
def drf_gsheet_qa(request):
    # Get Google Sheet Id
    # 取得 Google Sheet 的編號
    sheet_key = request.query_params.get('sheet_key', None)
    command = request.query_params.get('command', None)
    if sheet_key == None:
        return JsonResponse({
            'result': 'fail',
            'msg': 'Invalid Google sheet key.'
        })
    if command == None:
        return JsonResponse({
            'result': 'fail',
            'msg': 'Invalid command.'
        })
    machine_response = qa_sheet_machine(sheet_key, command)
    return JsonResponse({'msg': 'success', 'data': machine_response})


def qa_sheet_machine(sheet_key: str, command: str):
    ''' 
    Gets question and answer from Google work sheet.
    :sheet_key: Google work sheet key
    :command: 
       - !{tag value}         find some common questions by tag keyword. e.g !dinner
       - %{question id}       get the specific question and its answer. e.g ?1
       - +                    show all tags
       - ?                    help for how to use Q&A LINE bot.
    '''
    # Get one Google Sheet
    # 取得一個Google試算表
    if sheet_key == None:
        return JsonResponse({
            'result': 'fail',
            'msg': 'Invalid Google sheet key.'
        })
    # Get one Google Sheet
    # 取得一個Google試算表
    worksheet = gss_client.open_by_key(sheet_key).sheet1

    # Parse the command from LineBot
    # 解析從LineBot來的指令
    first_special_character = command[0]
    query_value = command[1:]

    if '!' == first_special_character:
        carousel_template_message = linebot_qa_msg(query_value)
        return carousel_template_message

    if '%' == first_special_character:
        index = int(query_value) + 1
        answer = worksheet.cell(index, 1).value + \
            " \n " + worksheet.cell(index, 3).value
        return {
            'result': 'success',
            'data': answer
        }

    if '~' == first_special_character:
        carousel_template_message = linebot_all_tags()
        return carousel_template_message

    if '?' == first_special_character:
        pass

    return {
        'result': 'success',
        'msg': "Q&A"
    }
